package com.hongshulin.dao.biz.blog;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.hongshulin.dao.core.BaseDao;
import com.hongshulin.model.biz.blog.BlogArticle;

@Repository
public class BlogArticleDao extends BaseDao<BlogArticle>
{
	@Override
	public Class<?> getEntityClass() {
		return BlogArticle.class;
	}
	
	public List getListByPage(PageBounds pageBounds) {
		String getStatement=getIbatisMapperNamespace() + SQL_GETALLLIST;
	    Map<String, Object> params = new HashMap<String, Object>();

		return this.getSqlSession().selectList(getStatement,params,pageBounds);
	}
}
