package com.hongshulin.dao.biz.blog;

import org.springframework.stereotype.Repository;

import com.hongshulin.dao.core.BaseDao;
import com.hongshulin.model.biz.blog.BlogArticleTag;
import com.hongshulin.model.biz.blog.BlogCategory;

@Repository
public class BlogCategoryDao extends BaseDao<BlogCategory>
{
	@Override
	public Class<?> getEntityClass() {
		return BlogArticleTag.class;
	}

}
