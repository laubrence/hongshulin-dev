package com.hongshulin.dao.biz.sys;

import org.springframework.stereotype.Repository;

import com.hongshulin.dao.core.BaseDao;
import com.hongshulin.model.biz.sys.SysUser;
import com.hongshulin.model.biz.test.TestModel;

@Repository
public class SysUserDao extends BaseDao<SysUser>
{
	@Override
	public Class<?> getEntityClass() {
		return TestModel.class;
	}

}
