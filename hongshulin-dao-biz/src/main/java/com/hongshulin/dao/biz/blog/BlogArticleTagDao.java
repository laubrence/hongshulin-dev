package com.hongshulin.dao.biz.blog;

import org.springframework.stereotype.Repository;

import com.hongshulin.dao.core.BaseDao;
import com.hongshulin.model.biz.blog.BlogArticleTag;

@Repository
public class BlogArticleTagDao extends BaseDao<BlogArticleTag>
{
	@Override
	public Class<?> getEntityClass() {
		return BlogArticleTag.class;
	}

}
