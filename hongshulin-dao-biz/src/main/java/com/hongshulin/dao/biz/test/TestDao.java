package com.hongshulin.dao.biz.test;

import org.springframework.stereotype.Repository;

import com.hongshulin.dao.core.BaseDao;
import com.hongshulin.model.biz.test.TestModel;

@Repository
public class TestDao extends BaseDao<TestModel>
{
	@Override
	public Class<?> getEntityClass() {
		return TestModel.class;
	}

}
