package com.hongshulin.dao.biz.blog;

import org.springframework.stereotype.Repository;

import com.hongshulin.dao.core.BaseDao;
import com.hongshulin.model.biz.blog.BlogArticleTag;
import com.hongshulin.model.biz.blog.BlogTag;

@Repository
public class BlogTagDao extends BaseDao<BlogTag>
{
	@Override
	public Class<?> getEntityClass() {
		return BlogArticleTag.class;
	}

}
