<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<exttitle>文章列表</title>
<extstatic>
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/bootstrap-datepicker/css/datepicker.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
</extstatic>
<extcontent>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			文章列表
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">主页</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">文章管理</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">添加文章</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>文章内容
							</div>
							
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="${ctx}/blog/article/save" id="form_sample_3" class="form-horizontal" method="POST">
								<input type="hidden" name="articleId" value="${blogArticle.articleId}">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">文章标题 <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="text" name="title" data-required="1" value="${blogArticle.title}" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">文章分类 <span class="required">* </span>
										</label>
										<div class="col-md-4">
											<select class="form-control select2me" name="categoryId">
												<option value="1">生活</option>
												<option value="2">技术</option>
												<option value="3">其他</option>
											</select>
										</div>
									</div>
									<div class="form-group last">
										<label class="control-label col-md-3">文章内容 <span class="required">
										* </span>
										</label>
										<div class="col-md-9">
											<!-- 加载编辑器的容器 -->
										    <script id="editor" name="content" style="height:500px;" type="text/plain">${blogArticle.content}</script>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">提交</button>
											<button type="button" class="btn default">取消</button>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
</extcontent>
	
<extscript>
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="${ctxStatic}/admin/pages/scripts/form-validation.js"></script>
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/lang/zh-cn/zh-cn.js"></script>
<!-- END PAGE LEVEL STYLES -->

<script>
	var ue = UE.getEditor('editor');
	
	ue.ready(function() {
	    //设置编辑器的内容
	   // ue.setContent("");
	});
</script>
</extscript>
	
	