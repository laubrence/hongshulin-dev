<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<title>文章列表</title>

<extstatic>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="${ctxStatic}/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="${ctxStatic}/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="${ctxStatic}/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="${ctxStatic}/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="${ctxStatic}/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

</extstatic>

<extcontent>
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			文章列表
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">主页</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">文章管理</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">文章列表</a>
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>文章列表
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
											<a href="${ctx}/blog/article/add" class="btn green">添加文章 <i class="fa fa-plus"></i></a>
										</div>
									</div>
								</div>
							</div>
							
							<table class="table table-striped table-bordered table-hover" id="sample_1">
								<thead>
								    <tr>
								        <th class="table-checkbox">
											<input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
										</th>
								        <th>标题</th>
								        <th>分类</th>
								        <th>创建时间</th>
								        <th>操作</th>
								    </tr>
								</thead>
								
							<!-- 
							<thead>
							<tr>
								
								<th>
									 序号
								</th>
								<th>
									 标题
								</th>
								<th>
									 分类
								</th>
								<th>
									 创建时间
								</th>
								<th>
									 操作
								</th>
							</tr>
							</thead>
							<tbody>
							
							<c:forEach var="bArticle" items="${bArticleList}">
								<tr class="odd gradeX">
									<td>
										<input type="checkbox" class="checkboxes" value="1"/>
									</td>
									<td>${bArticle.articleId}</td>
									<td>${bArticle.title}</td>
									<td>${bArticle.categoryId}</td>
									<td>${bArticle.createTime}</td>
									<td>
										<a href="#" class="btn default btn-xs green"> <i class="fa fa-list"></i> 查看  </a>
										<a href="${ctx}/blog/article/edit/${bArticle.articleId}" class="btn default btn-xs purple"><i class="fa fa-edit"></i> 编辑 </a>
										<a href="${ctx}/blog/article/delete/${bArticle.articleId}" class="btn default btn-xs red"><i class="fa fa-trash-o"></i> 删除 </a>
										<a href="#" class="btn default btn-xs blue"><i class="fa fa-share"></i> 分享 </a>
									</td>
								</tr>
							</c:forEach>
							 
							</tbody>
							-->
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

	
</extcontent>
<extscript>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="${ctxStatic}/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="${ctxStatic}/global/scripts/metronic.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/pages/scripts/table-managed.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {       
   	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
    TableManaged.init();
    
});
</script>
</extscript>
	
	