<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<exttitle>添加文章</exttitle>

<extstatic>

<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="${ctxStatic}/global/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="${ctxStatic}/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="${ctxStatic}/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="${ctxStatic}/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="${ctxStatic}/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css"/>
<link href="${ctxStatic}/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

</extstatic>
<extcontent>
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn blue">Save changes</button>
							<button type="button" class="btn default" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">
			文章列表
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">主页</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">文章管理</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">添加文章</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>文章内容
							</div>
							
						</div>
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<form action="${ctx}/blog/article/save" id="form_sample_3" class="form-horizontal" method="POST">
								<input type="hidden" name="articleId" value="${blogArticle.articleId}">
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3">文章标题 <span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<input type="text" name="title" data-required="1" value="${blogArticle.title}" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">摘要<span class="required">
										* </span>
										</label>
										<div class="col-md-4">
											<textarea class="form-control" rows="3" name="summary">${blogArticle.summary}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">文章分类 <span class="required">* </span>
										</label>
										<div class="col-md-4">
											<select class="form-control select2me" name="categoryId">
												<option value="1" <c:if test="${blogArticle.categoryId == 1}">selected="selected"</c:if>>生活</option>
												<option value="2" <c:if test="${blogArticle.categoryId == 2}">selected="selected"</c:if>>技术</option>
												<option value="3" <c:if test="${blogArticle.categoryId == 3}">selected="selected"</c:if>>其他</option>
											</select>
										</div>
									</div>
									<div class="form-group last">
										<label class="control-label col-md-3">文章内容 <span class="required">
										* </span>
										</label>
										<div class="col-md-9">
											<!-- 加载编辑器的容器 -->
										    <script id="editor" name="content" style="height:500px;" type="text/plain">${blogArticle.content}</script>
										</div>
									</div>
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">提交</button>
											<button type="cancel" class="btn default">取消</button>
										</div>
									</div>
								</div>
							</form>
							<!-- END FORM-->
						</div>
						<!-- END VALIDATION STATES-->
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</extcontent>

<extscript>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="${ctxStatic}/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/global/plugins/jquery-validation/js/additional-methods.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/global/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="${ctxStatic}/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL STYLES -->
<script src="${ctxStatic}/global/scripts/metronic.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/layout/scripts/demo.js" type="text/javascript"></script>
<script src="${ctxStatic}/admin/pages/scripts/form-validation.js"></script>

<script type="text/javascript">
	var context_ = "${ctx}";
</script>
<script src="http://api.html5media.info/1.1.4/html5media.min.js"></script>
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.parse.js"></script>
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.all.js"> </script>

<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/lang/zh-cn/zh-cn.js"></script>
<!-- END PAGE LEVEL STYLES -->
<script>
	jQuery(document).ready(function() {   
	   	// initiate layout and plugins
		Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		QuickSidebar.init(); // init quick sidebar
		Demo.init(); // init demo features
	   	FormValidation.init();
		
	});
</script>
<script>

	var ue = UE.getEditor('editor');

</script>
<!-- END JAVASCRIPTS -->
</extscript>