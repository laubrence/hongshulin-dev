var TableManaged = function () {

    var initTable1 = function () {

        var table = $('#sample_1');

        // begin first table
        table.dataTable({
            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
        	"processing": true,
            "serverSide": true,
            "ajax": "listpage",
            "columns": [
                 { "data": null},
                 { "data": "title" },
                 { "data": null },
                 { "data": "createTime" },
                 { "data": null }
             ],
             "columnDefs": [{
                 "render": function(data, type, row) {
                	 var str = "<div class=\"checker\"><span><input type=\"checkbox\" class=\"checkboxes\" value=\""+row.articleId+"\"></span></div>";
                     return str;
                 },
                 "targets": 0
             },{
                 "render": function(data, type, row) {                	 
                	 var categoryId = row.categoryId;
                	 var categoryName="";
                	 if(categoryId != ""){
                		 switch(categoryId){
                		 	case 1:categoryName="生活"; break;
                		 	case 2:categoryName="技术"; break;
                		 	case 3:categoryName="其他"; break;
                		 	default:categoryName="生活";
                		 }
                	 }
                	 return categoryName;
                 },
                 "targets": 2
             },{
                 "render": function(data, type, row) {              	 
                	 return timeStamp2String(row.createTime);
                 },
                 "targets": 3
             },{
                 "render": function(data, type, row) {                	 
                	 var str = "<a href=\"edit/"+row.articleId+"\" class=\"btn default btn-xs purple\"><i class=\"fa fa-edit\"></i>编辑 </a>"
                	 			+"<a href=\"delete/"+row.articleId+"\" class=\"btn default btn-xs red\"><i class=\"fa fa-trash-o\"></i> 删除 </a>";
                	 return str;
                     //return data + ' (' + row.article_id + ')';
                 },
                 "targets": 4
             }],
            "lengthMenu": [
                [10, 20, 50],
                [10, 20, 50] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,            
            "pagingType": "bootstrap_full_number",
            "searching": false,
            "ordering":  false,
            language: {
                "sProcessing": "处理中...",
                "sLengthMenu": "显示 _MENU_ 项结果",
                "sZeroRecords": "没有匹配结果",
                "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
                "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
                "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
                "sInfoPostFix": "",
                "sSearch": "搜索:",
                "sUrl": "",
                "sEmptyTable": "表中数据为空",
                "sLoadingRecords": "载入中...",
                "sInfoThousands": ",",
                "oPaginate": {
                    "sFirst": "首页",
                    "sPrevious": "上页",
                    "sNext": "下页",
                    "sLast": "末页"
                },
                "oAria": {
                    "sSortAscending": ": 以升序排列此列",
                    "sSortDescending": ": 以降序排列此列"
                }
            }
        });

        table.find('.group-checkable').change(function () {
            var set = jQuery(this).attr("data-set");
            var checked = jQuery(this).is(":checked");
            jQuery(set).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    $(this).parents('tr').addClass("active");
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('tr').removeClass("active");
                    $(this).parents('span').removeClass("checked");
                }
            });
            jQuery.uniform.update(set);
        });

        table.on('change', 'tbody tr .checkboxes', function () {
            $(this).parents('tr').toggleClass("active");
            $(this).parents('span').toggleClass("checked");
        });
        
//        //单机行，选中复选框
//        table.find("tr").slice(1).each(function(g){
//			var p = this;
//			$(this).children().slice(1).click(function(){
//				$($(p).children()[0]).children().each(function(){
//					if(this.type=="checkbox"){
//						if(!this.checked){
//							alert("ss");
//							this.checked = true;
//						}else{
//							this.checked = false;
//						}
//					}
//				});
//			});
//		});

    };

    return {

        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            initTable1();
        }

    };

}();
/**
 * @addon 时间戳(毫秒为单位)转换成 2016-3-15 14:58:16 格式
 * @param time
 * @returns {String}
 */
function timeStamp2String(time){
    var datetime = new Date();
    datetime.setTime(time);
    var year = datetime.getFullYear();
    var month = datetime.getMonth() + 1 < 10 ? "0" + (datetime.getMonth() + 1) : datetime.getMonth() + 1;
    var date = datetime.getDate() < 10 ? "0" + datetime.getDate() : datetime.getDate();
    var hour = datetime.getHours()< 10 ? "0" + datetime.getHours() : datetime.getHours();
    var minute = datetime.getMinutes()< 10 ? "0" + datetime.getMinutes() : datetime.getMinutes();
    var second = datetime.getSeconds()< 10 ? "0" + datetime.getSeconds() : datetime.getSeconds();
    return year + "-" + month + "-" + date+" "+hour+":"+minute+":"+second;
}