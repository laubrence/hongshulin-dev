package com.hongshulin.web.console.controller.blog;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.hongshulin.common.utils.VaildStringUtil;
import com.hongshulin.model.biz.blog.BlogArticle;
import com.hongshulin.service.biz.blog.BlogArticleService;
import com.hongshulin.web.console.core.utils.ResponseOutWithJson;

@Controller
@RequestMapping("/blog/article")
public class BlogArticleController {
	
	@Resource
	BlogArticleService blogArticleService;
	
	@RequestMapping(value="/add",method=RequestMethod.GET)
	public String articleToAdd(HttpServletRequest request){		
		return "blog/article_edit";
	}
	
	@RequestMapping(value="edit/{article_id}",method=RequestMethod.GET)
	public String articleUpdate(@PathVariable String article_id,HttpServletRequest request,Model model){
		if(!VaildStringUtil.isValidLong(article_id)){
			System.out.println("articleUpdate.isValidLong:"+article_id);
			return "500";
		}
		BlogArticle blogArticle = blogArticleService.getById(Long.valueOf(article_id));
		model.addAttribute("blogArticle", blogArticle);
	    return "blog/article_edit";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String articleAdd(HttpServletRequest request,BlogArticle blogArticle){
		if(blogArticle.getArticleId() == null){
			blogArticle.setArticleId(new Date().getTime()/1000);
			blogArticle.setCreateBy(1L);
			blogArticle.setCreateTime(new Date());
			blogArticle.setStatus((byte)1);
			blogArticle.setUpdateBy(1L);
			blogArticle.setUpdateTime(new Date());
			blogArticleService.add(blogArticle);
		}else{
			blogArticle.setUpdateBy(1L);
			blogArticle.setUpdateTime(new Date());
			blogArticleService.update(blogArticle);
		}
		
		return "redirect:/blog/article/list";
	}
	
	@RequestMapping(value="list",method=RequestMethod.GET) 
	public String articleList(HttpServletRequest request,Model model){
	    return "blog/article_list";       
	}
	
	@RequestMapping(value="listpage1",method=RequestMethod.GET) 
	public @ResponseBody Map articleListDatas(HttpServletRequest request){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ss", "value");
		return map;
	}
	
	@RequestMapping(value="listpage",method=RequestMethod.GET) 
	public void articleListData(HttpServletRequest request, HttpServletResponse response){
				
		String drawStr = request.getParameter("draw");
		int draw = 1;
		if(drawStr!=null && !"".equals(drawStr)){
			draw = Integer.valueOf(drawStr);
		}
		
		String startStr = request.getParameter("start");
		int start = 0;
		if(startStr!=null && !"".equals(startStr)){
			start = Integer.valueOf(startStr);
		}
		
		String lengthStr = request.getParameter("length");
		int length = 10;
		if(lengthStr!=null && !"".equals(lengthStr)){
			length = Integer.valueOf(lengthStr);
		}
		
		int page = (start/length)+1; //页号
		int pageSize = length; //每页数据条数
		String sortString = "create_time.desc";
		PageBounds pageBounds = new PageBounds(page, pageSize,Order.formString(sortString));
		List bArticleList = blogArticleService.getListByPage(pageBounds);
		
		PageList list  = (PageList)bArticleList;
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("draw", draw);
		map.put("recordsTotal", list.getPaginator().getTotalCount());
		map.put("recordsFiltered", list.getPaginator().getTotalCount());
		map.put("data", list);
		//map.put("error", "服务器异常");
		
		ResponseOutWithJson.responseToJson(response,map);
	}


	@RequestMapping(value="delete/{article_id}",method=RequestMethod.GET)
	public String articleDel(@PathVariable String article_id,HttpServletRequest request,Model model){
		blogArticleService.delById(Long.valueOf(article_id));
	    return "redirect:/blog/article/list";      
	}
}
