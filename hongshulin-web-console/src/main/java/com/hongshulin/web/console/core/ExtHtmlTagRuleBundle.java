package com.hongshulin.web.console.core;

import org.sitemesh.SiteMeshContext;
import org.sitemesh.content.ContentProperty;
import org.sitemesh.content.tagrules.TagRuleBundle;
import org.sitemesh.content.tagrules.html.ExportTagToContentRule;
import org.sitemesh.tagprocessor.State;

public class ExtHtmlTagRuleBundle implements TagRuleBundle {
    
    @Override
    public void install(State defaultState, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {
    	defaultState.addRule("exttitle", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("exttitle"), false));
    	defaultState.addRule("extstatic", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("extstatic"), false));
    	defaultState.addRule("extcontent", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("extcontent"), false));
    	defaultState.addRule("extscript", new ExportTagToContentRule(siteMeshContext, contentProperty.getChild("extscript"), false));
             
    }
     
    @Override
    public void cleanUp(State defaultState, ContentProperty contentProperty,
            SiteMeshContext siteMeshContext) {
         
    }
 
}