package com.hongshulin.service.core;

/**
 * 实现业务的基本操作类，实体主键为Long类型
 */
public abstract class BaseService<T> extends GenericService<T, Long> {

}

