package com.hongshulin.service.core;

import java.io.Serializable;
import java.util.List;

import com.hongshulin.dao.core.GenericDao;

/**
 * 服务基类。
 * 所有的业务实现类均需要从此类继承
 * @author laubrence
 *
 */
public abstract class GenericService <T, PK extends Serializable>{

	/**
	 * 需要被子类覆盖
	 * @return
	 */
	protected abstract GenericDao<T,PK> getGenericDao();

	/**
	 * 添加对象
	 * @param entity
	 */
	public void add(T entity){
		getGenericDao().add(entity);
	}

	/**
	 * 根据主键删除对象
	 * @param id
	 */
	public void delById(PK id){
		getGenericDao().delById(id);
	}

	/**
	 * 根据主键批量删除对象
	 * @param ids
	 */
	public void delByIds(PK[] ids){
		for (PK p : ids){
			delById(p);
		}
	}

	/**
	 * 修改对象
	 * @param entity
	 */
	public void update(T entity){
		getGenericDao().update(entity);
	}

	/**
	 * 根据主键Id获取对象
	 * @param id
	 * @return
	 */
	public T getById(PK id){
		return (T) getGenericDao().getById(id);
	}

	/**
	 * 返回所有记录
	 * @return
	 */
	public List<T> getAllList(){
		return getGenericDao().getAllList();
	}
	
}
