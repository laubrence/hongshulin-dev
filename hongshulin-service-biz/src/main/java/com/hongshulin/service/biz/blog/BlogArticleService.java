package com.hongshulin.service.biz.blog;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.hongshulin.dao.biz.blog.BlogArticleDao;
import com.hongshulin.dao.core.GenericDao;
import com.hongshulin.model.biz.blog.BlogArticle;
import com.hongshulin.service.core.BaseService;

@Service
public class BlogArticleService extends BaseService<BlogArticle>{
	@Resource
	private BlogArticleDao blogArticleDao;
	
	@Override
	protected GenericDao<BlogArticle, Long> getGenericDao() {
		return blogArticleDao;
	}
	
	/**
	 * 返回所有记录
	 * @return
	 */
	public List getListByPage(PageBounds pageBounds){
		
		//return null;
		return blogArticleDao.getListByPage(pageBounds);
	}
	
}


