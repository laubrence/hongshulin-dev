package com.hongshulin.dao.core;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;


/** 
 * @ClassName: BaseDao 
 * @Description: 数据库操作基类。<br> 包含基本的操作：增，查，改，删，列表，分页操作。
 * @author: laubrence
 * @date: 2015-12-23 下午5:06:12 
 * @param <T>
 * @param <PK> 
 */
public abstract class AbstractDao<T, PK extends Serializable> extends SqlSessionDaoSupport implements GenericDao<T, PK>{
	
	public static final String SQL_ADD = ".add";   
	public static final String SQL_UPDATE = ".update";   
	public static final String SQL_GETBYID = ".getById"; 
	public static final String SQL_DELETEBYID = ".delById"; 
	public static final String SQL_GETLIST = ".getList"; 
	public static final String SQL_GETALLLIST = ".getAllList"; 
	
	/**
	 * 注入sqlSessionFactory
	 * @param sqlSessionFactory
	 */
	@Resource
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory){
		super.setSqlSessionFactory(sqlSessionFactory);
	}
	
	/**
	 * 增加对象。
	 */
	public int add(T entity) {
		String addStatement=getIbatisMapperNamespace() + SQL_ADD;
		return this.getSqlSession().insert(addStatement, entity);
	}
	
	/**
	 * 修改对象。
	 */
	public int update(T entity) {
		String updateStatement=getIbatisMapperNamespace() + SQL_UPDATE;
		return this.getSqlSession().update(updateStatement, entity);
	}
	
    /**
     * 根据条件修改实体对象
     * @param map集合
     * @return 修改的对象个数
     */
    public int updateByParam(String sqlKey, Object param) {
		String updateStatement=getIbatisMapperNamespace() + "." + sqlKey;
		return this.getSqlSession().update(updateStatement, param);
    }
	
	/**
	 * 根据主键id获取数据
	 */
	@SuppressWarnings("unchecked")
	public T getById(PK primaryKey) {
		String getStatement=getIbatisMapperNamespace() + SQL_GETBYID;
		return (T)this.getSqlSession().selectOne(getStatement, primaryKey);
	}

	public int delById(PK primaryKey) {
		String delStatement=getIbatisMapperNamespace() + SQL_DELETEBYID;
		return this.getSqlSession().delete(delStatement, primaryKey);
	}

	public List<T> getList(Object param) {
		String getStatement=getIbatisMapperNamespace() + SQL_GETLIST;
		return this.getSqlSession().selectList(getStatement, param);
	}

	public List<T> getAllList() {
		String getStatement=getIbatisMapperNamespace() + SQL_GETALLLIST;
		return this.getSqlSession().selectList(getStatement);
	}
	
	/**
	 * 返回一条数据对象。
	 * @param sqlKey 。
	 * @param params 参数为单个对象或Map类型 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public T getUnique(String sqlKey,Object params){
		String getStatement= getIbatisMapperNamespace() + "." + sqlKey;
		T object = (T)this.getSqlSession().selectOne(getStatement,params);
		return object;
	}
	
	
	/**
	 * 返回单条数据，如 select count(*) from table_a 
	 * @param sqlKey
	 * @param params
	 * @return
	 */
	public Object getOne(String sqlKey,Object params){
		String statement=getIbatisMapperNamespace() + "." + sqlKey;
		Object object = this.getSqlSession().selectOne(statement,params);
		return object;
	}
	
	
	/**
	 * 获取当前dao 需要访问的model类名，在实现类中实现这个方法。<br>
	 * 主要的功能：用于获取类的完整路径，以提供给mybatis使用。
	 * @return
	 */
	public abstract Class getEntityClass();
	
	/**
	 * 根据出入的POJO对象获取ibatis的命名空间。<br>
	 * @return
	 */
	public String getIbatisMapperNamespace(){
		return getEntityClass().getName();
	}
	

}
